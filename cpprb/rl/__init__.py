from .algorithm import RandomPolicy,GreedyPolicy,EpsilonGreedyPolicy,SoftmaxPolicy
from .algorithm import DQN
